import { ALL } from './constants';

// eslint-disable-next-line import/prefer-default-export
export const isBaseFilterOption = id => id === ALL;
